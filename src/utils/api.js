import axios from 'axios';
import decode from 'jwt-decode';

let params={
        'type':'proceso_fase_1',
        'appointmentRequest':{
        'application':'SVL',
        'asnNumber':'23242323',
        'appointmentDate':'2019-04-05',
        'schedulingOptions':'1',
        'warehouseId':'944',
        'personId':'1111111-1',
        'name':'proveedor_1',
        'email':'person@company.com'
        },
        'items':[
        {
            'externalid':'1',
            'category':'J0101',
            'flow':'ALM',
            'type':'DOBLADO',
            'units':'300',
            'price':'10',
            'packageType':'UNIDAD',
            'line':'1001',
            'sla':'100',
            'vip':true,
            'size':'XL'
        }
        ]

}

let headers ={
    'x-country': 'CL',
    'x-business-id': '1',
    'Content-Type': 'application/json'
}

export function getOrden(orderId) {
    debugger;
    return axios.post('http://localhost:4000/api/proyecto/customer/orderId',{orderId});
}

export function getTiempo(request) {
    return axios.get('https://samples.openweathermap.org/data/2.5/weather',
    {
        params:{
            q: request.q,
            appid: request.appid
        }
    });
}


export function getLogin(request){
    debugger;
    return axios.post('http://localhost:4000/api/proyecto/customer/login',
                {
                    user: request.user, 
                    password: request.password 
                });
}

export function getAgenda(request){
    debugger;
    return axios.post('http://52.177.216.242:7000/bff/engine/api/v4/engine/reservation',params, headers);
}

export function auth(){

    let existeToken = sessionStorage.getItem('jwtToken');
    let request="";
 
    if(existeToken){
 
         console.log("existe token !!")
 
         if(!isTokenExpired(existeToken)){
             console.log("token valido !!")
             request = {
                     isAuth:true,
                     message:"",
                     token:"",
                     user:{
                         enabled: "",
                         username: getUsernameToken(existeToken)
                     }     
             }
         }else{
             console.log("token expiradooo !!")
             request = {
                     authorities:"",
                     isAuth:false,
                     message:"",
                     token:"",
                     user:{
                         enabled: false,
                         username:""
                     }    
             }
 
         }
 
    }else{
 
         console.log("no existe token !!")
         request = {
                 isAuth:false,
                 message:"",
                 token:"",
                 user:{
                     enabled: false,
                     username:""
                 }    
         }
 
    }
 
    return {
         type:'USER_AUTH',
         payload:request
    }
 
 }

 export function  isTokenExpired() {
    debugger;
    let existeToken = sessionStorage.getItem('jwtToken');
    try {
        let decoded = decode(existeToken);
        console.log(decode);
        var aux1= Date.now().getTime();
        var aux2 = decode.exp;
        if (decoded.exp< Date.now().getTime()) { // Checking if token is expired.
            return true;
        }else{
            return false;
        }
    }catch (err) {
        return false;
    }

}

function getUsernameToken(token){

    let username = "";

    try {
        const decoded = decode(token);
        username = decoded.sub
    }catch (err) {
        console.log(err);
        return ;
    }

    return username;

}
 
