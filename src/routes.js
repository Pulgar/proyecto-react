import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Inicio from './components/inicio'
import Resultado from './components/resultado';
import Empleados from './components/empleados';
import Formulario from './components/formulario';
import EjemploJSX from './components/ejemploJSX';
import InicioApiTiempo from './components/llamadaApiTiempo/inicio';
import ResultadoApiTiempo from './components/llamadaApiTiempo/resultado';
import Login from './components/login/login';
import OkLogin from './components/login/okLogin';


const Routes = () => {
    return (
            <Switch>
                <Route path="/inicio" exact component={Inicio}/>
				<Route path="/resultado" exact component={Resultado}/>
                <Route path="/empleados" exact component={Empleados}/>
                <Route path="/formulario" exact component={Formulario}/>
                <Route path="/ejemploJSX" exact component={EjemploJSX}/>
                <Route path="/inicioApiTiempo" exact component={InicioApiTiempo}/>
                <Route path="/resultadoApiTiempo" exact component={ResultadoApiTiempo}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/okLogin" exact component={OkLogin}/>
            </Switch>
    );
};

export default Routes;