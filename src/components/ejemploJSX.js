import React, { Component }  from 'react';

import lechuza from '../img/lechuza.jpg'


class EjemploJSX  extends Component {

    constructor(props) {
        super(props);
    }

    Icon = (
        <div className="App-body">
          <img 
            src={lechuza}
            className="App-image"
          />
        </div>);

    render(){return (this.Icon);}

}

export default EjemploJSX;
