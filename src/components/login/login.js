import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../../App.css';
import Header from '.././header/header';
import Footer from '.././header/footer';



export let request= {
    user: '',
    password: ''
}

class Login extends Component {

  state = {
      username:'',
      password:''
  }

  handleInputUsername = (event) => {
      this.setState({username:event.target.value})
  }
  handleInputPassword = (event) => {
      this.setState({password:event.target.value})
  }


  redirection(){ 
    this.props.history.push('/okLogin'); 
    }


  submitForm = (e) =>{
      e.preventDefault();
      request.user = this.state.username;
      request.password = this.state.password;
      this.redirection();
  }

  render() {
      return (
          <div className="App">
          <Header/>
          <body className="App-body">
              <form className="App-form" onSubmit={this.submitForm}>
                <label className="App-label">
                  Username: 
                      <input 
                          className = "App-input" 
                          type="email"
                          placeholder="Enter your username"
                          value={this.state.username}
                          onChange={this.handleInputUsername}
                          minlength="4" maxlength="30"
                          required
                      />
                </label>
                <label className="App-label">
                  Password:
                      <input 
                          className = "App-input" 
                          type="password"
                          placeholder="Enter your password"
                          value={this.state.password}
                          onChange={this.handleInputPassword}
                          minlength="4" maxlength="30"
						  required
                      />
                  </label>

                  <button className = "App-input-submit" type="submit">Log in</button>

              </form>
          </body>
          <Footer/>
          </div>
      );
  }
}

function mapStateToProps(state){
  return {
      user:state.token
  }
}

export default connect(mapStateToProps)(Login)

