import React, { Component } from 'react';
import '../../App.css';
import Header from '.././header/header';
import Footer from '.././header/footer';
import {request} from './login';
import { getLogin } from '../../utils/api.js';

export var token= ''

class OkLogin extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            email: '',
            password: '',
            error:'',
            token: '',
            success:false
		}
    }

    redirection(){ 
        this.props.history.push('/okLogin'); 
    }

    componentWillMount() {
		getLogin(request)
			.then((res) => {
                console.log(res);
                this.setState({
                    email: res.data.data.email,
                    password: res.data.data.password,
                    token: res.data.token,
                    success: true
                });
                
            })
            .catch((err) => console.log("ERRORRR"+err.message));

    }

    componentDidUpdate(){
        debugger;
        token = this.state.token;
    }
    render() {
        return (
            <div className="App">
        <Header/>
        <body className="App-body">
            <form  className="App-form" onSubmit={this.handleSubmit}>
                <label className="App-label">
                    Usuario: {this.state.email} 
                </label>
                <label className="App-label">
                    Password: {this.state.password} 
                </label>
                <label className="App-label">
                    token: {this.state.token} 
                </label>
            </form>
        </body>
        <Footer/>
        </div>
            );
    }
}

export default OkLogin;