import React, { Component } from 'react';
import '../App.css';
import {request} from './inicio';
import { getOrden } from '../utils/api.js';
import Header from './header/header'
import Footer from './header/footer'

class Resultado extends Component {
	
	constructor(props, context) {
		super(props, context);
		this.state = {
			customerId: '',
			customerUserId: '',
			customerFirstName: '',
			customerLastName: '',
			customerPhone: '',
			customerEmail: ''
		}
	}

	redirection(){ 
    this.props.history.push('/resultado'); 
	}
		
	componentDidMount() {
		getOrden(request)
			.then((res) => {
				this.setState({
					customerId: res.data.data.customer.customerId,
					customerUserId: res.data.data.customer.customerUserId,
					customerFirstName: res.data.data.customer.customerFirstName,
					customerLastName: res.data.data.customer.customerLastName,
					customerPhone: res.data.data.customer.customerPhone,
					customerEmail: res.data.data.customer.customerEmail
				});
			})
			.catch((err) => console.log(err));
			this.redirection();

	}


  render() {
    return (
	<div className="App">
		<header className="App-header">
		  <Header/>
		</header>
		<body className="App-body">
			<ul>Rut: {this.state.customerId}</ul>
			<ul>Id Usuario: {this.state.customerUserId}</ul>
			<ul>Nombre: {this.state.customerFirstName}</ul>
			<ul>Apellido: {this.state.customerLastName}</ul>
			<ul>Telefono: {this.state.customerPhone}</ul>
			<ul>Email: {this.state.customerEmail}</ul>
		</body>
		<footer>
			<Footer/>
		</footer>	
	</div>
    );
  }
}
 
export default Resultado;