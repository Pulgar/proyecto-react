import React, { Component } from 'react';
import '../../App.css';
import Header from '.././header/header';
import Footer from '.././header/footer';
import {request} from './inicio';
import { getAgenda } from '../../utils/api.js';


class ResultadoApiTiempo extends Component {

    constructor(props) {
        super(props);
        this.state = {
			idReservation: ''
		}
    }
    
    redirection(){ 
        this.props.history.push('/resultadoApiTiempo'); 
    }

    componentDidMount() {
		getAgenda(request)
			.then((res) => {
                console.log(res)
				this.setState({
					idReservation: res.data.appointments.idReservation
				});
			})
            .catch((err) => console.log("ERRORRR"+err.message));
			this.redirection();

	}

    render() {
        return (
            <div className="App">
        <Header/>
        <body className="App-body">
            <form  className="App-form" onSubmit={this.handleSubmit}>
                <label className="App-label">
                    Base: {this.state.idReservation} </label>
            </form>
        </body>
        <Footer/>
        </div>
            );
    }
}
export default ResultadoApiTiempo;