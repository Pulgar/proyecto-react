import React, { Component } from 'react';
import '../../App.css';
import Header from '.././header/header';
import Footer from '.././header/footer';

export let request = {
    q: '',
    appid: ''
};

class InicioApiTiempo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            q: '',
            appid: ''
          };

          this.handleChangeQ = this.handleChangeQ.bind(this);
          this.handleChangeAppid = this.handleChangeAppid.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChangeQ(event) {
        this.setState({q: event.target.value.toUpperCase()});
    }

    handleChangeAppid(event) {
        this.setState({appid: event.target.value.toUpperCase()});
    }

    handleSubmit(event) {
        request.q = this.state.q;
        request.appid = this.state.appid;
        event.preventDefault();
        this.redirection();
    }
    
    redirection(){ 
        this.props.history.push('/resultadoApiTiempo'); 
    }

    render() {
        return (
            <div className="App">
        <Header/>
        <body className="App-body">
            <form  className="App-form" onSubmit={this.handleSubmit}>
                <label className="App-label">
                    Ingrese Q: 
                    <input className = "App-input" type="text" value={this.state.q} onChange={this.handleChangeQ} />
                </label>
                <label className="App-label">
                    Ingrese Appid: 
                    <input className = "App-input" type="text" value={this.state.appid} onChange={this.handleChangeAppid} />
                </label>
                <input className = "App-input-submit" type="submit" value="Submit" />
            </form>
        </body>
        <Footer/>
        </div>
            );
    }
}
export default InicioApiTiempo;