import React, { Component } from 'react';
import '../App.css';
import Header from './header/header';
import Footer from './header/footer';
import Modal from 'react-responsive-modal';

export var request = "";

class Inicio extends Component {
	
	constructor(props, context) {
		super(props, context);	
		this.state = {
			orderId:'',
			open: false
			
		}
	}

	handleInputOrderId = (event) => {
        this.setState({orderId: event.target.value})
    }
	
	onOpenModal = () => {
    this.setState({ open: true });
	};
	 
	onCloseModal = () => {
		this.setState({ open: false });
	};
	
	redirection(){ 
    this.props.history.push('/resultado'); 
    } 

	submitForm = (e) =>{
		e.preventDefault();
		request = this.state.orderId;
		//this.onOpenModal();
		this.redirection();
	}
	
  render() {
	const { open } = this.state;
    return (
	<div className="App">
		<header className="App-header">
		  <Header/>
		</header>
		<body className="App-body">
			<form onSubmit={this.submitForm}>
				<label for="pedido">Ingrese número pedido:</label>
				<br />
				<input type="text" value={this.state.orderId} onChange={this.handleInputOrderId}/>
				<br />
				<button type="submit">Buscar</button>
			</form> 
			<Modal open={open} onClose={this.onCloseModal} center>
					<h4>{this.state.customerId}</h4>
			</Modal>
		</body>
		<footer>
			<Footer/>
		</footer>	
	</div>
    );
  }
}
export default Inicio;