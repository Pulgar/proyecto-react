import React, { Component } from 'react';
import '../App.css';
import Header from './header/header';
import Footer from './header/footer';
import {isTokenExpired} from '../utils/api'; 

class Formulario extends Component {
    constructor(props) {
      super(props);
      this.state = {
          valueInput: '',
          valueArea: '',
          valueSelect: '',
          isGoing: true,
          numberOfGuests: ''
        };
  
      this.handleChangeInput = this.handleChangeInput.bind(this);
      this.handleChangeArea = this.handleChangeArea.bind(this);
      this.handleChangeSelect = this.handleChangeSelect.bind(this);
      this.handleChangeCheck = this.handleChangeCheck.bind(this);
      this.handleChangeNumber = this.handleChangeNumber.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);

      
    }
  
    handleChangeInput(event) {
      this.setState({valueInput: event.target.value.toUpperCase()});
    }

    handleChangeArea(event) {
        this.setState({valueArea: event.target.value.toUpperCase()});
    }

    handleChangeSelect(event) {
        this.setState({valueSelect: event.target.value.toUpperCase()});
    }

    handleChangeCheck(event) {
        this.setState({isGoing: event.target.checked});
    }

    handleChangeNumber(event) {
        this.setState({numberOfGuests: event.target.value.toUpperCase()});
    }
  
    handleSubmit(event) {
      alert('A name was submitted: ' + this.state.valueInput + ' '+ this.state.valueArea + ' ' 
      + this.state.valueSelect + ' ' + this.state.isGoing + ' ' + this.state.numberOfGuests);
      event.preventDefault();
    }

    componentWillMount() {
      if(isTokenExpired()){
      this.props.history.push('/login')
      }
      else{
          this.props.history.push('/formulario')
      }

    }
  
    render() {
      return (
        <div className="App">
        <Header/>
        <body className="App-body">
          <form  className="App-form" onSubmit={this.handleSubmit}>
            <label className="App-label">
              Name: 
              <input className = "App-input" type="text" value={this.state.valueInput} onChange={this.handleChangeInput} />
            </label>
            <label className="App-label">
              Essay:
              <textarea className= "App-textarea" value={this.state.valueArea} onChange={this.handleChangeArea} />
            </label>
            <label className="App-label">
              Pick your favorite flavor: 
              <select className= "App-select" onChange={this.handleChangeSelect}>
                  <option value="grapefruit">Grapefruit</option>
                  <option value="lime">Lime</option>
                  <option value="coconut">Coconut</option>
                  <option value="mango">Mango</option>
              </select>
            </label>
            <label className="App-label">
              Is going:
              <input
                className="App-input-check"
                name="isGoing"
                type="checkbox"
                checked={this.state.isGoing}
                onChange={this.handleChangeCheck} />
            </label>
            <label className="App-label">
              Number of guests:
              <input
                className = "App-input"
                name="numberOfGuests"
                type="number"
                value={this.state.numberOfGuests}
                onChange={this.handleChangeNumber} />
            </label>
            <label className="App-label">
                File: <input className = "App-input-file" type="file" />
            </label>
            <input className = "App-input-submit" type="submit" value="Submit" />
          </form>
        </body>
        <Footer/>
        </div>
      );
    }
  }

  export default Formulario;